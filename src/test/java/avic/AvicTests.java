package avic;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import javax.swing.*;
import java.awt.*;


public class AvicTests {

    private WebDriver driver;

    @BeforeTest
    public void testSetup(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://avic.ua/");
    }

    @Test (priority = 1, description =
            "1. Open Avic, click on icon for login - registration offer is present" +
            "2. Click on Стать постоянным клиентом - registration page opened" +
            "3. Enter not valid phone number in registration form - validation appears phone is invalid")
    public void enterInvalidPhoneOnRegistrationPageTest(){

        driver.findElement(By.xpath("//div[contains(@class, \"middle-xs \")]//a[contains(@href,\"sign\")]")).click();
        String offerText = driver.findElement(By.xpath("//a[contains(@class, \"offers\")]")).getText();
        String expectedOffer = "Стать постоянным клиентом";
        Assert.assertEquals(offerText, expectedOffer);

        driver.findElement(By.xpath("//a[contains(@class, \"offers\")]")).click();
        String pageTitle = driver.findElement(By.xpath("//div[contains(@class, \"page-title\")]")).getText();
        String url = driver.getCurrentUrl();
        Assert.assertEquals(pageTitle, "Pегистрация");
        Assert.assertEquals(url, "https://avic.ua/sign-up");

        driver.findElement(By.xpath("//input[contains(@data-validate, \"phone\")]")).sendKeys("123456789");
        driver.findElement(By.xpath("//button[contains(@class, \"js_validate\")]")).click();
        String phoneError = driver.findElement(By.xpath("//div[contains(@data-error, \"телефон\")]")).getAttribute("data-error");
        Assert.assertEquals(phoneError, "Некорректный формат телефона");
    }


    @Test (priority = 2, description =
            "1. Open Avic, Apple Store > Iphone > iPhone 12 Mini" +
            "2. Buy specific iPhone, click Pay - checkout page opened" +
            "3. Verify that phone is correct")
    public void openIphone12MiniPageAndBuyOneTest(){

        Actions action = new Actions(driver);
        new WebDriverWait(driver, 30).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
        WebElement appleStore = driver.findElement(By.xpath("//a[contains(@href, \"apple-store\") and contains(@class, \"side\")]"));
        action.moveToElement(appleStore, 1, 1).perform();

        WebElement secondList = driver.findElement(By.xpath("//div[contains(@class,\"menu-lvl second-level\")]"));
        action.moveToElement(appleStore, 1, 1).perform();

        WebElement iPhone = driver.findElement(By.xpath("//a[contains(@href, \"iphone\") and contains(@class, \"side\")]"));
        action.moveToElement(iPhone,1, 1).perform();

        WebElement iPhone12Mini = driver.findElement(By.xpath("//li[contains(@class, \"single\")]//a[@href =\"/iphone/seriya--iphone-12-mini\"]"));
        action.moveToElement(iPhone12Mini, 1, 1).perform();

        driver.findElement(By.xpath("//li[contains(@class, \"single\")]//a[@href =\"/iphone/seriya--iphone-12-mini\"]")).click();
        new WebDriverWait(driver, 30).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
        driver.findElement(By.xpath("//a[contains(@data-ecomm-cart, \"MGDX3\")]")).click();
        WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("js_cart")));

        driver.findElement(By.xpath("//a[contains(@href, \"checkout\")]")).click();
        String itemToBuy = driver.findElement(By.xpath("//div[contains(@class, \"txt-h\")]//span[contains(text(), \"MGDX3\")]")).getText();

        Assert.assertEquals(itemToBuy, "Смартфон Apple iPhone 12 Mini 64GB Black (MGDX3)");
    }

    @Test (priority = 3, description =
            "1. Open pop-up for sending message - verify that form is opened" +
            "2. Send some text - verify that sent message shown" +
            "3. Close pop-up - verify that pop-up closed")
    public void sendMessageTest(){
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.className("hoverl_ec7e")));
        driver.findElement(By.xpath("//jdiv[@class = \"hoverl_ec7e\"]")).click();
        new WebDriverWait(driver, 10);
        String popupStatus1 = driver.findElement(By.xpath("//jdiv[@id=\"jcont\"]")).getAttribute("style");
        Assert.assertTrue(popupStatus1.contains("_OPEN_")); //verifying that pop-up is closed
        Assert.assertTrue(driver.findElement(By.xpath("//jdiv[@id=\"jcont\"]")).isDisplayed());   //verify that pop-up opened
        String textFieldXPath = "//textarea[contains(@class, \"inputField\")]";
        Assert.assertTrue(driver.findElement(By.xpath(textFieldXPath)).isEnabled());  //verify that input-field is active
        String greeting = driver.findElement(By.xpath("//textarea[contains(@class, \"inputField\")]")).getText(); //get greeting message
        String textToSend = "This is sendMessageTest test message";
        driver.findElement(By.xpath(textFieldXPath)).sendKeys(textToSend, Keys.ENTER);
        new WebDriverWait(driver, 5)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//jdiv[contains(@class, \"__client\") and starts-with(@class, \"message\")]")));
        Assert.assertTrue(driver.findElement(By.xpath("//jdiv[contains(@class, \"__client\") and starts-with(@class, \"message\")]")).isDisplayed());
        Assert.assertEquals(
                driver.findElement(By.xpath("//jdiv[contains(@class, \"__client\") and starts-with(@class, \"message\")]//jdiv")).getText(), textToSend);    //verify that text is exactly which was sent
        driver.findElement(By.xpath("//jdiv[contains(@class, \"closeIcon\")]")).click();
        new WebDriverWait(driver, 5);
        String popupStatus2 = driver.findElement(By.xpath("//jdiv[@id=\"jcont\"]")).getAttribute("style");
        Assert.assertTrue(popupStatus2.contains("_CLOSE_")); //verifying that pop-up is closed
    }

    @AfterTest
    public void shoutDownTest(){
        driver.quit();
    }
}
